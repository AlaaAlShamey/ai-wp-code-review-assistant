import { ChatOpenAI } from "@langchain/openai";
import { StringOutputParser } from "@langchain/core/output_parsers";
import { ChatPromptTemplate } from "@langchain/core/prompts";
import 'dotenv/config'

async function optimizeCode(model: ChatOpenAI, code: string): Promise<string> {
    const promptTemplate = ChatPromptTemplate.fromMessages([
        [
            "system",
            `
                Analyze the following WordPress code for syntax errors, potential bugs, and security vulnerabilities.
                Also, Check for adherence to coding standards and best practices by following WordPress phpcs rules in the following WordPress code.
                Also, Provide suggestions for improving the following WordPress code's readability, maintainability, and performance. Highlight areas where refactoring is needed.
                Also, Automatically generate or improve the documentation for the following WordPress code. Provide comments and explanations for complex code sections.
                Also, Detect and highlight code smells in the following WordPress code, such as duplicated code, long methods, large classes, etc. Suggest ways to refactor and improve the code structure.
                Also, Make sure to give me the full revised code with the response:
            `
        ],
        ["user", "```{code}```"],
    ]);

    const parser = new StringOutputParser();

    const chain = promptTemplate.pipe(model).pipe(parser);
    return await chain.invoke({code: code});
}

async function init() {
    const model = new ChatOpenAI({model: "gpt-4"});
    let response;

    // Example usage:
    const codeSample = `
        public static function get_post_ids(string $post_type): array
        {
            return get_posts(array(
                'posts_per_page' => -1,
                'fields' => 'ids',
                'post_type' => $post_type,
                'post_status' => 'publish'
            ));
        }
    `;

    response = await optimizeCode(model, codeSample);

    console.log(response);
}

init();